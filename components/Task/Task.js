import React, {useEffect, useRef, useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from "react-native";
import {Checkbox, IconButton} from "react-native-paper";
import LottieView from "lottie-react-native";
import Image from "../../assets/94220-tick-shake.json"


export default function Task(props) {
  const [checked, setChecked] = useState(false);
  const animation = useRef(null);

  useEffect(() => {
    animation.current?.play()
  }, []);

  return (
    <View style={styles.item}>
      <View style={styles.itemLeft}>
        {checked ?
          <TouchableOpacity onPress={() => {
            setChecked(prevState => !prevState)
          }}>
            <LottieView source={Image} speed={3}
                        style={{height: 39, width: 39}}
                        ref={animation}
                        loop={false}
                        autoPlay={true}/>
          </TouchableOpacity> : <Checkbox
            status={checked ? "checked" : "unchecked"}
            onPress={() => {
              setChecked(prevState => !prevState)
            }
            }/>
        }
        <Text style={styles.itemText}>{props.text}</Text>
      </View>
      <View
      >
        <View style={styles.iconWrapper}>
          <IconButton onPress={props.onDelete} icon={"delete"} size={25} color={"red"}
                      style={{backgroundColor: "#ffffff", borderColor: "red", borderWidth: 1}}/>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: "#ffffff",
    borderWidth: 1,
    padding: 15,
    borderRadius: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 20,
  },
  itemLeft: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
  },

  itemText: {
    maxWidth: "80%",
    marginLeft: 20,
    color: "#000000",
    fontSize: 20
  },
  iconWrapper: {
    flexDirection: "row",
    alignItems: "center"
  }
})
