const API_URL = 'https://3w5e27aa1m.execute-api.eu-central-1.amazonaws.com/dev/przemek/todo'

export const createItem = (txt, id) => fetch(API_URL + "/" + id, {
  method: "POST",
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({todo: txt})
});
export const getAllItems = () => fetch({method: 'GET', url: API_URL}, {});
export const removeItem = id => fetch({method: 'DELETE', url: API_URL + "/" + id}, {});
