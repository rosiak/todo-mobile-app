import React, {useEffect, useRef} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from "react-native";
import {StatusBar} from "expo-status-bar";
import NewTaskImg from "../assets/89089-work-and-life-balance.json";
import LottieView from "lottie-react-native";
import SeeTasksImg from "../assets/42293-get-things-done.json";


export default function Home({navigation}) {
  const animation = useRef(null);

  useEffect(() => {
    animation.current?.play()
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar style={"light"}/>
      <Text style={styles.screenTitle}>To Do App</Text>
      <View style={styles.menuWrapper}>
        <TouchableOpacity onPress={() => navigation.navigate("createTasks")}>
          <View style={styles.menuItem}>

            <LottieView source={SeeTasksImg}
                        style={{height: 150, width: 150}} ref={animation}
                        loop={true} autoPlay={true}/>
            <Text style={styles.menuText}>New Tasks</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate("tasksView")}>
          <View style={styles.menuItem}>
            <LottieView source={NewTaskImg}
                        style={{height: 150, width: 150}} ref={animation}
                        loop={true} autoPlay={true}/>
            <Text style={styles.menuText}>My Tasks</Text>
          </View>
        </TouchableOpacity>

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#041955',
    justifyContent: "center",
  },
  screenTitle: {
    color: 'white',
    fontSize: 50,
    textAlign: "center",
    marginBottom: 200
  },
  menuWrapper: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  menuItem: {
    borderWidth: 1,
    borderColor: "#ffffff",
    paddingHorizontal: 5,
    borderRadius: 10
  },
  menuText: {
    color: "#ffffff",
    textAlign: "center",
    fontSize: 30
  }
})
