import React, {useEffect, useRef, useState} from 'react';
import {Keyboard, KeyboardAvoidingView, Platform, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {StatusBar} from "expo-status-bar";
import LottieView from "lottie-react-native";
import Task from "../components/Task/Task";
import {FAB, IconButton, TextInput} from "react-native-paper";
import toDoImg from "../assets/95434-history.json";
import backImg from "../assets/lf20_pyskus9i.json";
import addImg from "../assets/lf20_nj9dnbwc.json";
import backgroundImg from "../assets/lf20_0crk0oba.json";
import {createItem} from "../api";

export default function CreateTasks({navigation}) {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [task, setTask] = useState("");
  const [taskItems, setTaskItems] = useState([]);
  const animation = useRef(null);


  useEffect(() => {
    animation.current?.play()
  }, [])

  const handleAddTask = () => {
    Keyboard.dismiss();
    console.log("ADD TASK")
    const taskId = new Date().getTime();
    setLoading(true);
    setError(null);
    createItem(task, taskId)
      .then(result => result.json())
      .then(result => {
        console.log({result});
        setTask(null);
        setTaskItems([...taskItems, result])
      })
      .catch(err => {
        console.error(err.toString());
        setError(err.toString());
      })
      .finally(() => {
        setLoading(false);
      })

  };

  const completeTask = (index) => {
    // removeItem(index) HERE DELETE ITEM
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1)
    setTaskItems(itemsCopy)
  }

  // const handleDelete = () => {
  //   let newItem = taskItems.filter(taskItem => taskItem.index !==taskItem.index)
  //   setTaskItems(newItem)
  // }

  return (
    <View style={styles.container}>
      <View>
        <LottieView source={backgroundImg}
                    loop={true}
                    autoPlay={true}
                    ref={animation}
                    style={styles.backgroundImg}
                    speed={.4}
        />
      </View>
      {/* Today tasks */}
      <StatusBar style={"light"}/>
      <View style={styles.taskWrapper}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => navigation.navigate("home")}>
            <LottieView
              disabled={!task}
              style={{height: 50, width: 50}}
              source={backImg}
              loop={true}
              autoPlay={true}
              ref={animation}
              speed={.8}/>
          </TouchableOpacity>
          <Text style={styles.sectionTitle}> New Task`s</Text>
          <LottieView source={toDoImg}
                      style={{height: 100, width: 100}} ref={animation}
                      loop={true} autoPlay={true}/>
        </View>
        <View style={styles.items}>
          {taskItems.map(({id, todo}) => <Task key={id} text={todo} onDelete={() => completeTask(id)}/>)}
          <View/>
        </View>
      </View>
      {/* Write task view */}
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.writeTaskWrapper}
      >
        <TextInput
          style={{width: "70%"}}
          value={task}
          activeOutlineColor={"#000000"}
          left={() => <IconButton icon={"keyboard"}/>}
          mode="outlined"
          onChangeText={task => setTask(task)}
          placeholder={"Add new task"}
          textAlign={"center"}
        />
        <View>
          {task ? <TouchableOpacity style={styles.addBtn} disabled={!task} onPress={handleAddTask}>
            <LottieView source={addImg}
                        loop={true}
                        autoPlay={true}
                        ref={animation}
            />
          </TouchableOpacity> : <FAB icon={"plus"} color={!task ? "black" : "green"} disabled={!task}
                                     style={{backgroundColor: !task ? "grey" : "#F9F871"}}
                                     onPress={handleAddTask}/>}
        </View>
      </KeyboardAvoidingView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#041955',
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomColor: "#ffffff",
    borderTopColor: "#041955",
    borderRightColor: "#041955",
    borderLeftColor: "#041955",
    borderWidth: 1
  },
  taskWrapper: {
    paddingTop: 80,
    paddingHorizontal: 20,
  },
  sectionTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'white',
    paddingBottom: 15
  },
  items: {
    marginTop: 30
  },
  writeTaskWrapper: {
    position: "absolute",
    bottom: 60,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  addWrapper: {
    width: 60,
    height: 60,
    borderRadius: 80,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#c0c0c0",
    borderWidth: 1
  },
  addBtn: {
    width: 80,
    height: 80
  },
  backgroundImg: {
    elevation: 3,
    height: 300, width: 300,
    zIndex: -1,
    position: "absolute",
    bottom: 0,
    top: 150,
    left: 20,
  }
});
