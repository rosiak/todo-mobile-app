import React, {useEffect, useRef} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from "react-native";
import LottieView from "lottie-react-native";
import backImg from "../assets/lf20_pyskus9i.json";
import listImg from "../assets/lf20_mdlggys3.json";
import backgroundImg from "../assets/lf20_0crk0oba.json";
import {fetch} from "react-native/Libraries/Network/fetch";


export default function TasksView({navigation}) {
  const animation = useRef(null);

  useEffect(() => {
    animation.current?.play()
  }, [])

  const getTasks = () => {
    fetch('https://3w5e27aa1m.execute-api.eu-central-1.amazonaws.com/dev/przemek/todo')
      .then(res => console.log(res))
  }


  return (
    <View style={styles.container}>
      <View>
        <LottieView source={backgroundImg}
                    loop={true}
                    autoPlay={true}
                    ref={animation}
                    style={styles.backgroundImg}
                    speed={.4}
        />
      </View>
      {/*<Text>{navigation.getParam("")}</Text>*/}
      <View style={styles.tasksWrapper}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => navigation.navigate("home")}>
            <LottieView
              style={{height: 50, width: 50}}
              speed={.8}
              source={backImg}
              loop={true}
              autoPlay={true}
              ref={animation}/>
          </TouchableOpacity>
          <Text style={styles.sectionTitle}>My Task`s</Text>
          <LottieView source={listImg}
                      style={{height: 80, width: 100}}
                      ref={animation}
                      loop={true} autoPlay={true}/>
        </View>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#041955",
    zIndex: 100
  },
  tasksWrapper: {
    paddingTop: 60,
    paddingHorizontal: 20
  },
  sectionTitle: {
    color: "#ffffff",
    textAlign: "center",
    fontSize: 20,
    fontWeight: 'bold',
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomColor: "#ffffff",
    borderTopColor: "#041955",
    borderRightColor: "#041955",
    borderLeftColor: "#041955",
    borderWidth: 1,
    paddingVertical: 10,
  },
  backgroundImg: {
    elevation: 3,
    height: 300, width: 300,
    zIndex: -1,
    position: "absolute",
    bottom: 0,
    top: 150,
    left: 20,
  }
})
