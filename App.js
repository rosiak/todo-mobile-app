import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import CreateTasks from "./screens/CreateTasks";
import TasksView from "./screens/TasksView";
import Home from "./screens/Home";


const Stack = createNativeStackNavigator();

export default function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{
        headerShown: false
      }}>
        <Stack.Screen name={"home"} component={Home}/>
        <Stack.Screen  name={"createTasks"} component={CreateTasks}/>
        <Stack.Screen name={"tasksView"} component={TasksView}/>
      </Stack.Navigator>
    </NavigationContainer>

  )
}
